// Menu Bar
$(document).ready(function () {
  

  $(".popup").hide();
  
  $(".img-menu").on("click", function () {
    $(".popup").fadeIn("slow");
    $("html, body").css({
      overflow: "hidden",
      height: "100%",
    });
    $(".popup-content").css({
      overflow: "scroll",
    });
  });
  $(".close-icon").on("click", function () {
    $(".popup").fadeOut("slow");
    $("html, body").css({
      overflow: "auto",
      height: "auto",
    });
  });
 
});
